from PIL import Image

infile = Image.open("sample.png")
width, height = infile.size
pixels = infile.load()
result = []
for x in range(width):
    for y in range(height):
        result.append(pixels[x, y])
print(result)
