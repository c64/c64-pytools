#!/usr/bin/python3

## Read png file,
## turn all pixels into a 2-bit-sequence depending on color
##   1st found color in image -> 00
##   2nd found color in image -> 01
##   ...
##   5th found color in image -> 00
##   6th found color in image -> 01
##   ...
## and write it out to file.
##
## You can also give an order of colors:
##
##   python3 smiley2bin.py in.png out.bin 8 0x0a 11 0
##
## would turn each pixel of
##   color 08  -> 00
##   color 10  -> 01
##   color 11  -> 10
##   color 0   -> 11
from PIL import Image, ImagePalette
import sys


def main():
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: hires_to_bin.py IMGFILE BINFILE '
            '[COLOR_00] [COLOR_01] [COLOR_10] [COLOR_11]')
    else:
        infile = sys.argv[1]
        outfile = sys.argv[2]
        # treat trailing args as numbers
        req_colors = [int(x, 0) for x in sys.argv[3:]]
    imgconv(infile, outfile, req_colors)
    return


def imgconv(infile, outfile, req_colors):
    im = Image.open(infile).copy()
    w, h = im.size
    pal = im.getpalette()
    print(pal)
    pixels = im.load()
    # read all pixels and collect colors of each pixel
    rgb_data = []
    for y in range(h):
        for x in range(w):
            rgb_data.append(pixels[x, y])
            print(pixels[x, y], end=" ")
        print()
    # get colors contained in image
    colors = list(set(rgb_data))  # something like [0, 8, 11, 1]
    print("Colors in input img: %s" % colors)
    # reorder contained colors by order given on commandline
    colors.sort(key=(req_colors + colors).index)
    print("Colors reordered due to args: %s" % colors)
    # turn all pixels into a sequence of bytes
    result = 0  # expect to grow a looong number
    for pixel in rgb_data:
        to_add = colors.index(pixel) & 3  # color -> %00, %01, %10 or %11
                                          # cut off any higher bits (4, 8, ...)
        result = (result << 2) | to_add  # left-shift result by 2 bits
                                         # and add current 2 bits
    print(bin(result))
    byte_seq = result.to_bytes(int(len(rgb_data)/4), "big")
    open(outfile, "wb").write(byte_seq)


if __name__ == "__main__":
    main()
