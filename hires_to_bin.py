#!/usr/bin/env python

from PIL import Image, ImagePalette
from color_distances import (
        C64_COLOR_DISTANCES, RGB_COLORS, RGB_COLOR_LIST, get_c64_palette)
import sys
from collections import Counter
from math import *
import os.path


def main():

    if len(sys.argv) < 3:
        sys.exit('Usage: hires_to_bin.py [IMGFILE] [BINFILE]')
    else:
        infile = sys.argv[1]
        outfile = sys.argv[2]

    imgtochars(infile, outfile)

    return


def imgtochars(infile, outfile):

    # Open image file and convert to C64 bitmap data

    imagefile = Image.open(infile).copy()

    width, height = imagefile.size
    if width != 320 or height != 200:
        sys.exit('Image dimensions need to be exactly 320 x 200')

    cols = int(width / 8)
    rows = int(height / 8)

    output = []
    bitmap = []
    colors = []
    error_count = 0

    # Reduce to 16 colors
    imagefile = imagefile.quantize(colors=16)
    imagefile.save("output1.png")  # just to check result...

    ### Adapt to palette
    #print(imagefile.getpalette())
    #new_palette = get_c64_palette(imagefile.getpalette())
    ##new_palette = ImagePalette.ImagePalette(
    ##        mode='RGB', palette=RGB_COLOR_LIST, size=768)
    #imagefile.putpalette(new_palette)
    #imagefile.save("output.png")
    #return

    # Divide image into characters
    pal = imagefile.getpalette()
    c64_pal = get_c64_palette(pal)
    for row in range(rows):
        for col in range(cols):
            # Calculate char region
            rect = (8 * col, 8 * row, 8 * (col + 1), 8 * (row + 1))
            # Crop char region so we can mess with it
            char = imagefile.crop(rect)
            # Get the pixel values
            pixels = char.load()  # get the pixel values (palette indexes)
            indices = []

            # Examine the pixels in each character
            for y in range(8):
                for x in range(8):
                    p = pixels[x, y]
                    # List all the palette indices so we can count them later
                    indices.append(p)
            # Count colors in character
            c = list(Counter(indices))

            # Low nibble (background)
            color_bg = RGB_COLORS.index(tuple(c64_pal)[3 * c[0]: 3* c[0] + 3])

            # High nibble (foreground)
            color_fg = color_bg
            if len(list(c)) > 1:
                color_fg = RGB_COLORS.index(
                        tuple(c64_pal)[3 * c[1]: 3* c[1] + 3])

            # Combine the two colors in a single byte
            colors.append((color_fg << 4) | color_bg)

            # colors set for this char, now determine the bitmap...
            for y in range(8):
                byte = 0
                for x in range(8):
                    # get the C64 color of each pixel...
                    color_pix = RGB_COLORS.index(
                            tuple(c64_pal)[
                                3 * pixels[x, y]: 3 * pixels[x, y] + 3])
                    # check whether it is more similar to fg or bg...
                    dist = C64_COLOR_DISTANCES[color_pix]
                    if dist.index(color_fg) > dist.index(color_bg):
                        b = 0
                    else:
                        b = 1
                    byte = byte | (b << (7 - x))

                # Char row completed
                bitmap.append(byte)

            # Update character with new pixel values
            imagefile.paste(char, rect)

    # Save 1-bit image (to show any errors)
    outname = 'check_' + infile
    imagefile.save(outname)
    print('Error check: check_' + infile)

    # Save binary files
    outbin = open('bitmap_' + outfile, 'wb')
    outbin.write(bytes(bitmap))
    outbin.close()
    print('Bitmap data: bitmap_' + outfile)

    outcol = open('colors_' + outfile, 'wb')
    outcol.write(bytes(colors))
    outcol.close()
    print('Color data: colors_' + outfile)
    return


if __name__ == "__main__":
    main()
