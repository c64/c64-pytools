#!/usr/bin/env python
import math

# RGB representations of C64 colors
# courtesy of http://unusedino.de/ec64/technical/misc/vic656x/colors/
RGB_COLORS = (
        (0x00, 0x00, 0x00),  #  0 - black
        (0xff, 0xff, 0xff),  #  1 - white
	(0x68, 0x37, 0x2b),  #  2 - red
	(0x70, 0xa4, 0xb2),  #  3 - cyan
	(0x6f, 0x3d, 0x86),  #  4 - purple
	(0x58, 0x8d, 0x43),  #  5 - green
	(0x35, 0x28, 0x79),  #  6 - blue
	(0xB8, 0xC7, 0x6F),  #  7 - yellow
	(0x6F, 0x4F, 0x25),  #  8 - orange
	(0x43, 0x39, 0x00),  #  9 - brown
	(0x9A, 0x67, 0x59),  # 10 - light red
	(0x44, 0x44, 0x44),  # 11 - dark grey
	(0x6C, 0x6C, 0x6C),  # 12 - grey
	(0x9A, 0xD2, 0x84),  # 13 - light green
	(0x6C, 0x5E, 0xB5),  # 14 - light blue
	(0x95, 0x95, 0x95)   # 15 - light grey
        )


# The colors as a simple list (can be used as palette by PIL)
RGB_COLOR_LIST = [val for rgb in RGB_COLORS for val in rgb]

RGB_COLOR_LIST = RGB_COLOR_LIST + 240 * 3 * [0]


def color_diff(rgb1, rgb2):
    """Get distance of two rgb colors (3-tuples)

      Result is based on SimpleCoders answer on
      https://stackoverflow.com/questions/9018016/how-to-compare-two-colors-for-similarity-difference
    """
    r1, g1, b1 = rgb1
    r2, g2, b2 = rgb2
    return math.sqrt((r2-r1)**2 + (g2-g1)**2 + (b2-b1)**2)
    r_mean = (r1 + r2) / 2.0
    r, g, b, = r1 - r2, g1 - g2, b1 - b2
    w = (
            2 + r_mean / 256.0,
            4.0,
            2 + (256.0 - r_mean) / 256)
    return math.sqrt(w[0] * r * r + w[1] * g * g + w[2] * b * b)



def c64_color_distances():
    """Get color distances of all C64 colors to each other color.

       A list of 16 lists, each one containing the "most similar" colors
       ordered. For intance (1, 5, 7, 3, ...) would mean that color number
       1 (first element) is most similar to color 1, after that most similar
        to color number 5, and so on.

       Similarity computed based on color_diff() (see above).
    """
    distances = []
    for n1, c1 in enumerate(RGB_COLORS):
        d = sorted([(color_diff(c1, x), n) for n, x in enumerate(RGB_COLORS)])
        distances.append(tuple([x[1] for x in d]))
    return distances


C64_COLOR_DISTANCES = c64_color_distances()


def get_c64_palette(rgb_palette):
    """Compute a C64 color palette from a given one.
    """
    values = int(len(rgb_palette) / 3)
    result = []
    for n in range(values - 1):
        (r, g, b) = list(rgb_palette)[3*n:3*n+3]
        diffs = sorted([(color_diff((r, g, b), c), c) for c in RGB_COLORS])
        nearest = diffs[0][1]
        result.extend(nearest)
    return result
